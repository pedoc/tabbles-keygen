﻿namespace TabblesKeygen
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.cbxProduct = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpExpireAt = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumberOfLicense = new System.Windows.Forms.TextBox();
            this.txtLicenseKeys = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGen = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxProduct
            // 
            this.cbxProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProduct.FormattingEnabled = true;
            this.cbxProduct.Items.AddRange(new object[] {
            "TabblesPro5",
            "TabblesCorp5",
            "TabblesBasic5",
            "TabblesStudent5",
            "ConfidentialCorp1",
            "ConfidentialBus1",
            "ConfidentialBasic1",
            "TabblesBasic4"});
            this.cbxProduct.Location = new System.Drawing.Point(83, 38);
            this.cbxProduct.Name = "cbxProduct";
            this.cbxProduct.Size = new System.Drawing.Size(111, 20);
            this.cbxProduct.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "产品：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(200, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "过期时间：";
            // 
            // dtpExpireAt
            // 
            this.dtpExpireAt.Location = new System.Drawing.Point(272, 38);
            this.dtpExpireAt.Name = "dtpExpireAt";
            this.dtpExpireAt.Size = new System.Drawing.Size(153, 21);
            this.dtpExpireAt.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "公司标记：";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(83, 80);
            this.txtCompany.MaxLength = 2;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(109, 21);
            this.txtCompany.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(198, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "许可数量：";
            // 
            // txtNumberOfLicense
            // 
            this.txtNumberOfLicense.Location = new System.Drawing.Point(272, 83);
            this.txtNumberOfLicense.MaxLength = 2;
            this.txtNumberOfLicense.Name = "txtNumberOfLicense";
            this.txtNumberOfLicense.Size = new System.Drawing.Size(153, 21);
            this.txtNumberOfLicense.TabIndex = 7;
            this.txtNumberOfLicense.Text = "10";
            // 
            // txtLicenseKeys
            // 
            this.txtLicenseKeys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLicenseKeys.Location = new System.Drawing.Point(3, 17);
            this.txtLicenseKeys.Multiline = true;
            this.txtLicenseKeys.Name = "txtLicenseKeys";
            this.txtLicenseKeys.Size = new System.Drawing.Size(508, 296);
            this.txtLicenseKeys.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLicenseKeys);
            this.groupBox1.Location = new System.Drawing.Point(28, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 316);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "序列号";
            // 
            // btnGen
            // 
            this.btnGen.Location = new System.Drawing.Point(450, 83);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(92, 23);
            this.btnGen.TabIndex = 10;
            this.btnGen.Text = "生成(&S)";
            this.btnGen.UseVisualStyleBackColor = true;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(560, 139);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(153, 55);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "请将 127.0.0.1 srv.tag-forge.com 加入hosts";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 451);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnGen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtNumberOfLicense);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpExpireAt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxProduct);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "License Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxProduct;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpExpireAt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumberOfLicense;
        private System.Windows.Forms.TextBox txtLicenseKeys;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.TextBox textBox1;
    }
}

