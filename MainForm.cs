﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TabblesKeygen
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbxProduct.SelectedIndex = 0;
            txtCompany.Text = RandomCompany(2);
            dtpExpireAt.Value=DateTime.Now.AddYears(30);
        }

        private void Gen()
        {
            if (int.TryParse(txtNumberOfLicense.Text, out var numberOfLicense) && numberOfLicense > 0)
            {
                for (int i = 0; i < numberOfLicense; i++)
                {
                    var body = $"{cbxProduct.SelectedItem.ToString()}-ShareawareOnSale{DateTime.Now:MMddyy}-{i}-NND{dtpExpireAt.Value:MM/dd/yyyy}-N";
                    var sign = GenSign(body);
                    var licenseKey = $"4-{body}-{sign}";
                    txtLicenseKeys.Text += licenseKey + Environment.NewLine+ Environment.NewLine;
                }
            }
        }

        private string GenSign(string body)
        {
            var count = 9;
            var sb = new StringBuilder();
            for (int i = 0; i <= count; i++)
            {
                var item = Licensing.serialGenerateSuffix2(body, i);
                sb.Append(item);
            }
            return sb.ToString();
        }

        private string RandomCompany(int count)
        {
            var seed = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var rand = new Random();
            var result = new char[count];
            for (int i = 0; i < count ; i++)
            {
                var index = rand.Next(0, seed.Length);
                result[i] = seed[index];
            }
            return new string(result);
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            Gen();
        }

        private void btnOemFrom_Click(object sender, EventArgs e)
        {
            new OemForm().ShowDialog(this);
        }
    }
}
