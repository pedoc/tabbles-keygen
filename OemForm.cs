﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dnlib.DotNet;
using dnlib.DotNet.Resources;
using dnlib.IO;
using TabblesKeygen.Entity;

namespace TabblesKeygen
{
    public partial class OemForm : Form
    {
        private ModuleDefMD module;
        public OemForm()
        {
            InitializeComponent();
        }

        private void btnLoadAssembly_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog() { Filter = @"exe|*.exe|dll|*.dll", CheckFileExists = true };
            if (ofd.ShowDialog(this) == DialogResult.OK) txtAssemblyPath.Text = ofd.FileName;
            ofd.Dispose();
        }

        private void OemForm_Load(object sender, EventArgs e)
        {
#if DEBUG
            txtAssemblyPath.Text = @"E:\Crack\Yellow Blue Soft\Tabbles\TaggerLibCSharp.dll";
#endif
        }

        private void LoadAssembly()
        {
            var assemblyPath = txtAssemblyPath.Text;
            if (string.IsNullOrEmpty(assemblyPath) || !System.IO.File.Exists(assemblyPath))
            {
                MessageBox.Show(@"程序集路径为空或文件不存在，请重新指定");
                return;
            }
            module?.Dispose();
            module = ModuleDefMD.Load(assemblyPath);
            if (module == null)
            {
                MessageBox.Show(@"程序集加载失败，请检查是否是.NET程序");
                return;
            }
            ShowLog($"{module.Name} 加载完成");
        }

        private void ShowLog(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return;
            else
                tssl.Text = msg;
        }

        private void btnLoadAssembly_Click_1(object sender, EventArgs e)
        {
            LoadAssembly();
        }

        private void btnLoadPlan_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog() { Filter = @"json|*.json", CheckFileExists = true };
            if (ofd.ShowDialog(this) == DialogResult.OK)
            {
                txtPlanPath.Text = ofd.FileName;
                txtPlanContent.Text = File.ReadAllText(txtPlanPath.Text, Encoding.UTF8);
            }
            ofd.Dispose();
        }

        private void btnSavePlan_Click(object sender, EventArgs e)
        {
            var plan = txtPlanPath.Text;
            if (string.IsNullOrEmpty(plan) || !File.Exists(plan))
            {
                ShowLog("方案路径不存在，无法保存");
                return;
            }
            else
            {
                File.WriteAllText(plan, txtPlanContent.Text, Encoding.UTF8);
                ShowLog("方案保存成功");
            }
        }

        private Oem LoadOem(string txt)
        {
            if (string.IsNullOrEmpty(txt)) return null;
            try
            {
                var oem = Newtonsoft.Json.JsonConvert.DeserializeObject<Oem>(txt);
                return oem;
            }
            catch (Exception ex)
            {
                ShowLog($"加载方案时出错，详情={ex.Message}");
                return null;
            }
        }

        private void btnExecutePlan_Click(object sender, EventArgs e)
        {
            var planContent = txtPlanContent.Text;
            if (string.IsNullOrEmpty(planContent))
            {
                ShowLog("方案内容不能为空");
                return;
            }

            var oem = LoadOem(planContent);
            if (oem == null)
            {
                ShowLog("方案不具有任何有效规则");
                return;
            }
            var dic = new Dictionary<string, EmbeddedResource>();
            //   var resx = module.Resources.FindEmbeddedResource("TaggerLibCSharp.Resource1.resources");
            var errors = new List<PlanItem>();
            foreach (var planItem in oem.Plans)
            {
                if (module.Name != oem.Assembly)
                {
                    ShowLog($"{planItem.Key} 所在 {oem.Assembly} 与当前模块不匹配（{module.Name}）");
                    continue;
                }
                if (!dic.ContainsKey(oem.ResourceName))
                {
                    var res = module.Resources.FindEmbeddedResource(oem.ResourceName);
                    if (res == null)
                    {
                        ShowLog($"在 {oem.Assembly} 中不存在名为 {oem.ResourceName} 的资源");
                        continue;
                    }
                    dic[oem.ResourceName] = res;
                }
                var resourceElementSet = ResourceReader.Read(module, dic[oem.ResourceName].Data);
                var resourceElement = resourceElementSet.ResourceElements.FirstOrDefault(i => i.Name == planItem.Key.Trim());
                if (resourceElement == null)
                {
                    errors.Add(planItem);
                    ShowLog($"在 {oem.Assembly} 中不存在Key为 {oem.ResourceName} 的资源");
                    continue;
                }

                resourceElement.ResourceData = new BuiltInResourceData(ResourceTypeCode.String, planItem.NewVal);
                var ms = new MemoryStream();
                ResourceWriter.Write(module, ms, resourceElementSet);
                dic[oem.ResourceName].Data = new MemoryImageStream(0, ms.GetBuffer(), 0, ms.GetBuffer().Length);
            }

            var path = Path.GetDirectoryName(txtAssemblyPath.Text);
            var fileName = Path.GetFileNameWithoutExtension(txtAssemblyPath.Text);
            var ext = Path.GetExtension(txtAssemblyPath.Text);
            module.Write(Path.Combine(path, fileName + ".patched." + ext));
        }
    }
}
