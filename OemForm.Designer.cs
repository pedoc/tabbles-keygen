﻿namespace TabblesKeygen
{
    partial class OemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAssemblyPath = new System.Windows.Forms.TextBox();
            this.txtPlanPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPlanContent = new System.Windows.Forms.TextBox();
            this.btnBrowserAssembly = new System.Windows.Forms.Button();
            this.btnLoadPlan = new System.Windows.Forms.Button();
            this.btnExecutePlan = new System.Windows.Forms.Button();
            this.btnSavePlan = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssl = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnLoadAssembly = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "程序集：";
            // 
            // txtAssemblyPath
            // 
            this.txtAssemblyPath.Location = new System.Drawing.Point(80, 25);
            this.txtAssemblyPath.Name = "txtAssemblyPath";
            this.txtAssemblyPath.Size = new System.Drawing.Size(308, 21);
            this.txtAssemblyPath.TabIndex = 1;
            // 
            // txtPlanPath
            // 
            this.txtPlanPath.Location = new System.Drawing.Point(80, 69);
            this.txtPlanPath.Name = "txtPlanPath";
            this.txtPlanPath.Size = new System.Drawing.Size(308, 21);
            this.txtPlanPath.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "方案：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPlanContent);
            this.groupBox1.Location = new System.Drawing.Point(23, 113);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(689, 313);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "方案详情";
            // 
            // txtPlanContent
            // 
            this.txtPlanContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPlanContent.Location = new System.Drawing.Point(3, 17);
            this.txtPlanContent.Multiline = true;
            this.txtPlanContent.Name = "txtPlanContent";
            this.txtPlanContent.Size = new System.Drawing.Size(683, 293);
            this.txtPlanContent.TabIndex = 0;
            // 
            // btnBrowserAssembly
            // 
            this.btnBrowserAssembly.Location = new System.Drawing.Point(414, 25);
            this.btnBrowserAssembly.Name = "btnBrowserAssembly";
            this.btnBrowserAssembly.Size = new System.Drawing.Size(92, 23);
            this.btnBrowserAssembly.TabIndex = 5;
            this.btnBrowserAssembly.Text = "浏览(&B)";
            this.btnBrowserAssembly.UseVisualStyleBackColor = true;
            this.btnBrowserAssembly.Click += new System.EventHandler(this.btnLoadAssembly_Click);
            // 
            // btnLoadPlan
            // 
            this.btnLoadPlan.Location = new System.Drawing.Point(414, 68);
            this.btnLoadPlan.Name = "btnLoadPlan";
            this.btnLoadPlan.Size = new System.Drawing.Size(92, 23);
            this.btnLoadPlan.TabIndex = 6;
            this.btnLoadPlan.Text = "加载方案(&L)";
            this.btnLoadPlan.UseVisualStyleBackColor = true;
            this.btnLoadPlan.Click += new System.EventHandler(this.btnLoadPlan_Click);
            // 
            // btnExecutePlan
            // 
            this.btnExecutePlan.Location = new System.Drawing.Point(512, 68);
            this.btnExecutePlan.Name = "btnExecutePlan";
            this.btnExecutePlan.Size = new System.Drawing.Size(92, 23);
            this.btnExecutePlan.TabIndex = 7;
            this.btnExecutePlan.Text = "执行方案(&E)";
            this.btnExecutePlan.UseVisualStyleBackColor = true;
            this.btnExecutePlan.Click += new System.EventHandler(this.btnExecutePlan_Click);
            // 
            // btnSavePlan
            // 
            this.btnSavePlan.Location = new System.Drawing.Point(610, 68);
            this.btnSavePlan.Name = "btnSavePlan";
            this.btnSavePlan.Size = new System.Drawing.Size(92, 23);
            this.btnSavePlan.TabIndex = 8;
            this.btnSavePlan.Text = "保存方案(&S)";
            this.btnSavePlan.UseVisualStyleBackColor = true;
            this.btnSavePlan.Click += new System.EventHandler(this.btnSavePlan_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 429);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(724, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssl
            // 
            this.tssl.Name = "tssl";
            this.tssl.Size = new System.Drawing.Size(0, 17);
            // 
            // btnLoadAssembly
            // 
            this.btnLoadAssembly.Location = new System.Drawing.Point(512, 25);
            this.btnLoadAssembly.Name = "btnLoadAssembly";
            this.btnLoadAssembly.Size = new System.Drawing.Size(92, 23);
            this.btnLoadAssembly.TabIndex = 10;
            this.btnLoadAssembly.Text = "加载(&A)";
            this.btnLoadAssembly.UseVisualStyleBackColor = true;
            this.btnLoadAssembly.Click += new System.EventHandler(this.btnLoadAssembly_Click_1);
            // 
            // OemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 451);
            this.Controls.Add(this.btnLoadAssembly);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnSavePlan);
            this.Controls.Add(this.btnExecutePlan);
            this.Controls.Add(this.btnLoadPlan);
            this.Controls.Add(this.btnBrowserAssembly);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtPlanPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAssemblyPath);
            this.Controls.Add(this.label1);
            this.Name = "OemForm";
            this.Text = "OemForm";
            this.Load += new System.EventHandler(this.OemForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAssemblyPath;
        private System.Windows.Forms.TextBox txtPlanPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPlanContent;
        private System.Windows.Forms.Button btnBrowserAssembly;
        private System.Windows.Forms.Button btnLoadPlan;
        private System.Windows.Forms.Button btnExecutePlan;
        private System.Windows.Forms.Button btnSavePlan;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssl;
        private System.Windows.Forms.Button btnLoadAssembly;
    }
}