﻿namespace TabblesKeygen.Entity
{
    public class PlanItem
    {
        public string Key { get; set; }
        public string RawVal { get; set; }
        public string NewVal { get; set; }
        public ResType ResType { get; set; }
    }
}
