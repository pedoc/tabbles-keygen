﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TabblesKeygen.Entity
{
    public class Oem
    {
        public string Title { get; set; }
        public string Company { get; set; } = "Yellow blue soft";
        public string Product { get; set; } = "Tabbles";
        public string Copyright { get; set; } = "Copyright ©  2017 Yellow blue soft";
        public string Version { get; set; } = "4.4.10.0";

        public string Assembly { get; set; }
        public string ResourceName { get; set; }
        public List<PlanItem> Plans { get; set; }
    }
}
